i-dotcom sso package
********************
Simple sso client/server package to create your own sso/openid client/server.

Documentation::

    $ cd doc; make html

or try:
 * http://doc.i-shark.com/pypi/idotcom.sso
