from django.conf.urls.defaults import  patterns


urlpatterns = patterns(
    'idotcom.sso.server.views',
    (r'^$', 'server'),
    (r'^xrds/$', 'idpXrds'),
    (r'^processTrustResult/$', 'processTrustResult'),
    (r'^id/(?P<shop>[\w\-_]+)/$', 'idPage'),
    (r'^id/$', 'idPage'),
    (r'^endpoint/$', 'endpoint'),
    (r'^trust/$', 'trustPage'),
)
