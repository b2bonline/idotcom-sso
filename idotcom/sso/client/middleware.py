from idotcom.sso.client.utils import get_or_create_user_by_sreg
from django.contrib.auth import authenticate, login

class RegisterOrAuthenticateUserByOpenID(object):
    """
    Check for valid request.openid and authenticate user using sreg.nickname
    or register new user.

    Requires:
        AUTHENTICATION_BACKENDS = (
            ...
            'django.contrib.auth.backends.RemoteUserBackend',
        )
    """

    def process_request(self, request):
        if not request.user.is_authenticated():
            if request.openid:
                user = get_or_create_user_by_sreg(request.openid.openid, request.openid.sreg)
                if user:
                    ruser = authenticate(remote_user=user.username)
                    if ruser:
                        login(request, ruser)
                        #Dodanie do sesji GCI firmy z B2Bid (dotyczy Salespanel)
                        #GCI wysylane jest przez niewykorzystywane wczesniej pole "postcode"
                        try:
                            request.session['gci'] = request.openid.sreg.get('postcode')
                        except:
                            pass

        else:
            if request.openid:
                request.session['openids'] = []
