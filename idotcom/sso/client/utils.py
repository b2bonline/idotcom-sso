from django.contrib.auth.models import User
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings


def get_or_create_user_by_sreg(openid, sreg):
    try:
        valid_openids = settings.OPENID_VALID_OPENIDS
    except AttributeError:
        raise ImproperlyConfigured('Cannot find settings.OPENID_VALID_OPENIDS list')

    if openid not in valid_openids:
        return None

    login = sreg.get('nickname')
    user, created = User.objects.get_or_create(username=login, defaults = {
        'username': login,
        'email': sreg.get('email'),
    })
    user.set_unusable_password()
    user.save()
    return user



