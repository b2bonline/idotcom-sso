idotcom.sso.client
==================

Dependency
----------
 * `django-openid-consumer <http://code.google.com/p/django-openid-consumer/>`_

Configuration
-------------

To create your own openid client you need:
 * add **'django_openid_consumer.middleware.OpenIDMiddleware'**
   and **'idotcom.sso.client.middleware.RegisterOrAuthenticateUserByOpenID'** ,to
   your **MIDDLEWARE_CLASSES**
 * set your 
    **AUTHENTICATION_BACKENDS** = ( 
        'django.contrib.auth.backends.RemoteUserBackend',
        'django.contrib.auth.backends.ModelBackend',
        )
 * add to your **urls.py**: **(r'^openid/', include('django_openid_consumer.urls'))**


You have to configure openid:
 * **OPENID_SREG = {"requred": "nickname, email", "optional":"postcode, country", "policy_url": "http://example.com/policy"}**
 * **OPENID_PAPE = {"policy_list": "http://schemas.openid.net/pape/policies/2007/06/multi-factor", "max_auth_age": 3600}**
 * **OPENID_VALID_OPENIDS = ( 'http://your.openid.server/server/id/',  )**
 * **OPENID_DEFAULT_OPENID = 'http://your.opend.server/server/id/'** 


Templates
---------
If you want to use included sample templates:
 * add **'idotcom.sso.client'** to your **INSTALLED_APPS**
