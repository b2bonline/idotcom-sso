idotcom.sso code documentation
******************************

.. automodule:: idotcom.sso


Package idotcom.sso.server
==========================

.. automodule:: idotcom.sso.server
   :members:

Package idotcom.sso.client
==========================

.. automodule:: idotcom.sso.client
   :members:


