Usage
=====

Dependency
----------
idotcom.sso depends on `python-openid <http://pypi.python.org/pypi/python-openid/>`_

Packages
--------

.. toctree::
    :maxdepth: 2

    server
    client
