idotcom.sso.server
==================

Configuration
-------------

To create your own openid server you need:
 * add to your **urls.py**: **(r'^server/', include('idotcom.sso.server.urls'))**

Optionaly you can disable yes/no decide page by:
 * adding OPENID_SHOWDECIDE_PAGE = False
in your settings

Templates
---------

If you want to use included sample templates:
 * add **'idotcom.sso.server'** to your **INSTALLED_APPS**
