.. idotcom.sso documentation master file, created by
   sphinx-quickstart on Wed Feb  2 11:46:25 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to idotcom.sso's documentation!
=======================================

.. toctree::
    :maxdepth: 2

    usage
    code


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

